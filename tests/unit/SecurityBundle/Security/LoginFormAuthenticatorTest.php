<?php

namespace Unit\SecurityBundle\Security;

use Codeception\Test\Unit;
use Kisphp\SecurityBundle\Model\AdminUserModel;
use Kisphp\SecurityBundle\Security\LoginFormAuthenticator;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * @group security
 */
class LoginFormAuthenticatorTest extends Unit
{
    /**
     * @var LoginFormAuthenticator
     */
    protected $lfa;

    protected function setUp() : void
    {
        $form = \Mockery::mock(Form::class);
        $form->shouldReceive('handleRequest')->andReturnNull();
        $form->shouldReceive('getData')->andReturn([
            '_username' => 'admin@example.com',
            '_password' => 'admin',
        ]);

        $formFactory = \Mockery::mock(FormFactoryInterface::class);
        $formFactory->shouldReceive('create')->withAnyArgs()->andReturn($form);

        $aum = \Mockery::mock(AdminUserModel::class);
        $aum->shouldReceive('findOneBy')->withAnyArgs()->andReturnNull();

        $ri = \Mockery::mock(RouterInterface::class);
        $ri->shouldReceive('generate')->withAnyArgs()->andReturn('/generated/url');

        $this->lfa = new LoginFormAuthenticator($formFactory, $aum, $ri);

        parent::setUp();
    }

    public function test_getCredentials_on_login()
    {
        $session = \Mockery::mock(Session::class);
        $session->shouldReceive('set')->withAnyArgs()->andReturnNull();

        $request = \Mockery::mock(Request::class);
        $request->shouldReceive('getPathInfo')->withNoArgs()->andReturn('/login');
        $request->shouldReceive('isMethod')->with('POST')->andReturnTrue();
        $request->shouldReceive('getSession')->andReturn($session);

        $this->assertSame([
            '_username' => 'admin@example.com',
            '_password' => 'admin',
        ], $this->lfa->getCredentials($request));
    }

    public function test_getCredentials_not_on_login()
    {
        $session = \Mockery::mock(Session::class);
        $session->shouldReceive('set')->withAnyArgs()->andReturnNull();

        $request = \Mockery::mock(Request::class);
        $request->shouldReceive('getPathInfo')->withNoArgs()->andReturn('/homepage');
        $request->shouldReceive('isMethod')->with('POST')->andReturnTrue();
        $request->shouldReceive('getSession')->andReturn($session);

        $this->assertNull($this->lfa->getCredentials($request));
    }

    public function test_getUser()
    {
        $up = \Mockery::mock(UserProviderInterface::class);

        $this->assertNull($this->lfa->getUser(['_username' => 'admin@example.com'], $up));
    }

    public function test_checkCredentials()
    {
        $user = \Mockery::mock(UserInterface::class);

        $this->assertTrue($this->lfa->checkCredentials(['_password' => 'admin'], $user));
    }

    public function test_getDefaultSuccessRedirectUrl()
    {
        $this->assertSame('/generated/url', $this->lfa->getDefaultSuccessRedirectUrl());
    }
}
