<?php

namespace Unit\SecurityBundle\Security;

use Codeception\Test\Unit;
use Kisphp\SecurityBundle\Security\ApiUser;

/**
 * @group security
 */
class ApiUserTest extends Unit
{
    public function testApiUser()
    {
        $api = new ApiUser('api_user');

        $this->assertSame('api_user', $api->getUsername());
        $this->assertSame(['ROLE_API'], $api->getRoles());
        $this->assertNull($api->getPassword());
        $this->assertNull($api->getSalt());
        $this->assertNull($api->eraseCredentials());
    }
}
