<?php

namespace Unit\SecurityBundle\Model;

use Codeception\Test\Unit;
use Doctrine\ORM\EntityManagerInterface;
use Kisphp\SecurityBundle\Entity\AdminUser;
use Kisphp\SecurityBundle\Model\AdminUserModel;
use Knp\Component\Pager\Paginator;

/**
 * @group security
 */
class AdminUserModelTest extends Unit
{
    public function testEntityType()
    {
        $em = \Mockery::mock(EntityManagerInterface::class);
        $pag = \Mockery::mock(Paginator::class);

        $model = new AdminUserModel($em, $pag);

        $this->assertInstanceOf(AdminUser::class, $model->createEntity());
    }
}
