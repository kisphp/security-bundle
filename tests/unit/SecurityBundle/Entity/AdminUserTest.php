<?php

namespace Unit\SecurityBundle\Entity;

use Codeception\Test\Unit;
use Kisphp\SecurityBundle\Entity\AdminUser;
use SecurityBundle\Helper\TestUser;

/**
 * @group security
 */
class AdminUserTest extends Unit
{
    public function testAdminUsers()
    {
        $user = new TestUser();
        $user->setId(1);
        $user->setName('name');
        $user->setRoles(['ROLE_DEMO']);
        $user->setPassword('password');
        $user->setUsername('username');
        $user->setEmail('admin@example.com');

        $user->updateModifiedDatetime();

        $this->assertInstanceOf(AdminUser::class, $user);

//        $this->assertSame(['ROLE_DEMO'], $user->getRoles());
        $this->assertSame(1, $user->getId());
        $this->assertSame('name', $user->getName());
        $this->assertSame('password', $user->getPassword());
        $this->assertSame(2, $user->getStatus());
        $this->assertSame('admin@example.com', $user->getUsername());
        $this->assertSame('admin@example.com', $user->getEmail());
        $this->assertSame('name', $user->getFullname());

        $this->assertInstanceOf(\DateTime::class, $user->getRegistered());
    }

    public function testUserNoRoles()
    {
        $user = new TestUser();
        $user->setRoles([]);

        $this->assertSame(['ROLE_ADMIN'], $user->getRoles());
    }

    public function testUserStringRoles()
    {
        $user = new TestUser();
        $user->setRoles(['ROLE_ADMIN','ROLE_USER']);

        $this->assertSame(['ROLE_ADMIN', 'ROLE_USER'], $user->getRoles());
    }

    public function testSerialize()
    {
        $user = new TestUser();
        $user->setId(1);
        $user->setEmail('admin@example.com');

        $serialized = serialize($user);

        $this->assertStringContainsString('admin@example.com', $serialized);
    }
}
