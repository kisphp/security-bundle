# Kisphp Security Bundle

Provide administrators management and login into application 

## Installation

```sh
composer require kisphp/security-bundle
```

Register new bundle in AppKernel.php file in your symfony2 project

```php
$bundles = array(
    ...
    new Kisphp\SecurityBundle\KisphpSecurityBundle(),
);
```

Add admin_roles array in `app/conf/parameters.yaml` file

```yaml
admin_roles:
    - ROLE_USER
    - ROLE_ADMIN
    - ROLE_SUPER_ADMIN
```
