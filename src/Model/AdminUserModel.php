<?php

namespace Kisphp\SecurityBundle\Model;

use AppBundle\Entity\AdminUser;
use Kisphp\FrameworkAdminBundle\Model\AbstractModel;

class AdminUserModel extends AbstractModel
{
    const REPOSITORY = 'AppBundle:AdminUser';

    /**
     * @return \AppBundle\Entity\AdminUser|\Kisphp\Entity\KisphpEntityInterface|\Kisphp\FrameworkAdminBundle\Entity\AdminUser
     */
    public function createEntity()
    {
        return new AdminUser();
    }
}
