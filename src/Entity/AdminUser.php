<?php

namespace Kisphp\SecurityBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Kisphp\Entity\KisphpEntityInterface;
use Kisphp\Entity\ToggleableInterface;
use Kisphp\Utils\Status;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\MappedSuperclass()
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\Entity
 * @ORM\Table(name="admin_users", options={"collate": "utf8_general_ci", "charset": "utf8"})
 * @ORM\HasLifecycleCallbacks()
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"adminuser": "AppBundle\Entity\AdminUser"})
 */
abstract class AdminUser implements UserInterface, KisphpEntityInterface, ToggleableInterface
{
    /**
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer", options={"default": Kisphp\Utils\Status::ACTIVE})
     */
    protected $status = Status::ACTIVE;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $registered;

    /**
     * @var string
     * @ORM\Column(type="string", length=64)
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, unique=true)
     */
    protected $email;

    /**
     * @var string
     * @ORM\Column(type="string", length=32)
     */
    protected $roles = 'ROLE_ADMIN';

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    protected $password;

    /**
     * @var ArrayCollection
     */
    protected $password_form;

    /**
     * @var ArrayCollection
     */
    protected $account_form;

    public function __construct()
    {
        $this->password_form = new ArrayCollection();
        $this->account_form = new ArrayCollection();
    }

    /**
     * @return array
     */
    public function __sleep()
    {
        return ['id', 'email'];
    }

    /**
     * @ORM\PrePersist()
     */
    public function updateModifiedDatetime()
    {
        if ($this->getRegistered() === null) {
            $this->setRegistered(new \DateTime());
        }
    }

    /**
     * @return string
     */
    public function getFullname()
    {
        return $this->getName();
    }

    /**
     * @param array $roles
     */
    public function setRoles(array $roles)
    {
        $this->roles = implode(',', $roles);
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        $roles = $this->roles;
        if (is_string($roles) && !empty($this->roles)) {
            $roles = explode(',', $this->roles);
        }

        if (empty($roles)) {
            $roles = ['ROLE_ADMIN'];
        }

        return $roles;
    }

    /**
     * @return null|string|void
     */
    public function getSalt()
    {
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->email;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function eraseCredentials()
    {
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return \DateTime
     */
    public function getRegistered()
    {
        return $this->registered;
    }

    /**
     * @param \DateTime $registered
     */
    public function setRegistered($registered)
    {
        $this->registered = $registered;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return ArrayCollection
     */
    public function getPasswordForm()
    {
        return $this->password_form;
    }

    /**
     * @param ArrayCollection $password_form
     */
    public function setPasswordForm($password_form)
    {
        $this->password_form = $password_form;
    }

    /**
     * @return ArrayCollection
     */
    public function getAccountForm()
    {
        return $this->account_form;
    }

    /**
     * @param ArrayCollection $account_form
     */
    public function setAccountForm($account_form)
    {
        $this->account_form = $account_form;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}
