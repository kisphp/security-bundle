<?php

namespace Kisphp\SecurityBundle\DemoData;

use Kisphp\FrameworkAdminBundle\Fixtures\AbstractDemoData;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class AdminDemoData extends AbstractDemoData
{
    /**
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @throws \Exception
     */
    public function loadDemoData(InputInterface $input, OutputInterface $output)
    {
        $this->createUsers($output);
    }

    /**
     * @param OutputInterface $output
     */
    protected function createUsers(OutputInterface $output)
    {
        $model = $this->getContainer()->get('model.admin_user');

        $superAdmin = $model->createEntity();
        $superAdmin->setName('Super Admin');
        $superAdmin->setEmail('admin@example.com');
        $superAdmin->setPassword($this->encodePassword($superAdmin, 'admin'));
        $superAdmin->setRoles(['ROLE_SUPER_ADMIN']);

        $admin = $model->createEntity();
        $admin->setName('Admin');
        $admin->setEmail('user@example.com');
        $admin->setPassword($this->encodePassword($admin, 'user'));
        $admin->setRoles(['ROLE_ADMIN']);

        $guest = $model->createEntity();
        $guest->setName('Guest');
        $guest->setEmail('guest@example.com');
        $guest->setPassword($this->encodePassword($guest, 'guest'));
        $guest->setRoles(['ROLE_USER']);

        $model->save($superAdmin);
        $model->save($admin);
        $model->save($guest);

        $output->writeln('<info>Created admin@example.com</info>');
        $output->writeln('<info>Created user@example.com</info>');
        $output->writeln('<info>Created guest@example.com</info>');
    }

    /**
     * @param UserInterface $user
     * @param string $password
     *
     * @return string
     */
    protected function encodePassword(UserInterface $user, $password)
    {
        return $this->getContainer()
            ->get('security.password_encoder')
            ->encodePassword($user, $password)
        ;
    }
}
