<?php

namespace Kisphp\SecurityBundle\Form\User;

use Symfony\Component\Form\FormBuilderInterface;

class UserPasswordForm extends AdminUserAbstractForm
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->createPasswordField($builder);
    }
}
