<?php

namespace Kisphp\SecurityBundle\Form\User;

use Kisphp\SecurityBundle\Entity\AdminUser;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;

class AdminUserCreateForm extends AdminUserAbstractForm
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->createNameField($builder);
        $this->createEmailField($builder);
        $this->createPasswordField($builder);
        $this->createRolesSelectionField($builder, $options);

        $this->applyTransformer($builder, $options);
    }

    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param array $options
     */
    protected function applyTransformer(FormBuilderInterface $builder, array $options)
    {
        $builder->addModelTransformer(new CallbackTransformer(
            function ($entity) {
                return $this->transformDbToEntity($entity);
            },
            function ($entity) use ($options) {
                return $this->transformEntityToDb($entity, $options);
            }
        ));
    }

    /**
     * @param \Kisphp\FrameworkAdminBundle\Entity\AdminUser $adminUser
     *
     * @return \Kisphp\FrameworkAdminBundle\Entity\AdminUser
     */
    protected function transformDbToEntity(AdminUser $adminUser)
    {
        return $adminUser;
    }

    /**
     * @param \Kisphp\FrameworkAdminBundle\Entity\AdminUser $adminUser
     *
     * @return \Kisphp\FrameworkAdminBundle\Entity\AdminUser
     */
    protected function transformEntityToDb(AdminUser $adminUser)
    {
        return $adminUser;
    }
}
