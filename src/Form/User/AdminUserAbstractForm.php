<?php

namespace Kisphp\SecurityBundle\Form\User;

use AppBundle\Entity\AdminUser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

abstract class AdminUserAbstractForm extends AbstractType
{
    /**
     * @param OptionsResolver $resolver
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'attr' => [
                'novalidate' => 'novalidate',
            ],
            'data_class' => AdminUser::class,
        ]);

        $resolver->setRequired('current_user');
        $resolver->setRequired('admin_roles');
    }

    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     */
    protected function createNameField(FormBuilderInterface $builder)
    {
        $builder->add('name', TextType::class, [
            'constraints' => [
                new NotBlank(),
            ],
        ]);
    }

    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     */
    protected function createEmailField(FormBuilderInterface $builder)
    {
        $builder->add('email', EmailType::class, [
            'constraints' => [
                new NotBlank(),
                new Email(),
            ],
        ]);
    }

    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     */
    protected function createPasswordField(FormBuilderInterface $builder)
    {
        $builder->add('password', RepeatedType::class, [
            'type' => PasswordType::class,
            'invalid_message' => 'The password fields must match.',
            'options' => [
                'attr' => [
                    'class' => 'password-field',
                ],
            ],
            'required' => true,
            'first_options' => [
                'label' => 'Password',
            ],
            'second_options' => [
                'label' => 'Repeat Password',
            ],
        ]);
    }

    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     */
    protected function createRolesSelectionField(FormBuilderInterface $builder, array $options)
    {
        if (in_array('ROLE_SUPER_ADMIN', $options['current_user']->getRoles()) === false) {
            return;
        }

        $builder->add('roles', ChoiceType::class, [
            'label' => 'Account type',
            'multiple' => true,
            'choices' => $this->createRolesSelectChoices($options['admin_roles']),
        ]);
    }

    /**
     * @param array $adminRoles
     * @return array
     */
    protected function createRolesSelectChoices(array $adminRoles)
    {
        if (empty($adminRoles)) {
            return [
                'User' => 'ROLE_USER',
                'Admin' => 'ROLE_ADMIN',
                'Super Admin' => 'ROLE_SUPER_ADMIN',
            ];
        }

        $roles = [];
        foreach ($adminRoles as $role) {
            $roles[$this->filterRoleKey($role)] = $role;
        }

        return $roles;
    }

    /**
     * @param string $key
     * @return string
     */
    protected function filterRoleKey($key)
    {
        $key = str_replace('ROLE_', '', $key);
        $key = strtolower($key);
        $key = str_replace('_', ' ', $key);

        return ucwords($key);
    }
}
