<?php

namespace Kisphp\SecurityBundle\Form\User;

use Symfony\Component\Form\FormBuilderInterface;

class AdminUserEditForm extends AdminUserAbstractForm
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->createNameField($builder);
        $this->createEmailField($builder);
        $this->createRolesSelectionField($builder, $options);
    }
}
