<?php

namespace Kisphp\SecurityBundle\Services\Menu;

use Kisphp\FrameworkAdminBundle\Services\MenuItem;

class AdminMenuItems extends MenuItem
{
    const ROLES = 'ROLE_SUPER_ADMIN';

    /**
     * @param \ArrayIterator $iterator
     */
    public function getMenuItems(\ArrayIterator $iterator)
    {
        $iterator->append([
            'is_header' => true,
            'valid_feature' => 'admin',
            'label' => 'Admin',
        ]);
        $iterator->append([
            'is_header' => false,
            'valid_feature' => 'admin',
            'match_route' => 'admin',
            'path' => 'adm_admin_user',
            'icon' => 'fa-users',
            'label' => 'main_navigation.admin_users',
        ]);
    }
}
