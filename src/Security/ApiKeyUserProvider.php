<?php

namespace Kisphp\SecurityBundle\Security;

use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class ApiKeyUserProvider implements UserProviderInterface
{
    const API_USERNAME = 'apiname';

    /**
     * @param string $apiKey
     *
     * @return string
     */
    public function getUsernameForApiKey($apiKey)
    {
        return self::API_USERNAME;
    }

    /**
     * @param string $username
     *
     * @return ApiUser
     */
    public function loadUserByUsername($username)
    {
        return new ApiUser($username);
    }

    /**
     * @param UserInterface $user
     *
     * @return UserInterface|void
     */
    public function refreshUser(UserInterface $user)
    {
        throw new UnsupportedUserException();
    }

    /**
     * @param string $class
     *
     * @return bool
     */
    public function supportsClass($class)
    {
        return ApiUser::class === $class;
    }
}
