<?php

namespace Kisphp\SecurityBundle\Controller;

use Kisphp\FrameworkAdminBundle\Controller\AbstractController;
use Kisphp\SecurityBundle\Form\User\AdminUserCreateForm;
use Kisphp\SecurityBundle\Form\User\AdminUserEditForm;
use Kisphp\SecurityBundle\Form\User\UserPasswordForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Template()
 */
class AdminUserController extends AbstractController
{
    const SECTION_TITLE = 'section.title.admin_user';
    const MODEL_NAME = 'model.admin_user';
    const LISTING_TEMPLATE = '@KisphpSecurity/AdminUser/index.html.twig';

    /**
     * @var array
     */
    protected $section = [
        self::EDIT_PATH => 'adm_admin_user_edit',
        self::LIST_PATH => 'adm_admin_user',
        self::ADD_PATH => 'adm_admin_user_add',
        self::STATUS_PATH => 'adm_admin_user_status',
        self::DELETE_PATH => 'adm_admin_user_delete',
    ];

    /**
     * @param Request $request
     * @param int $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, $id = 0)
    {
        $currentUser = $this->getUser();
        $entity = $this->get(static::MODEL_NAME)->find($id);

        if ($id > 0 && !$entity) {
            $this->addFlash('error', 'User does not exists!');
            return $this->redirect(
                $this->generateUrl('adm_admin_user_edit', ['id' => $currentUser->getId()])
            );
        }

        if (in_array('ROLE_SUPER_ADMIN', $currentUser->getRoles()) === false && $currentUser->getId() !== $entity->getId()) {
            $this->addFlash('error', 'You do not have permissions to edit other user!');
            return $this->redirect(
                $this->generateUrl('adm_admin_user_edit', ['id' => $currentUser->getId()])
            );
        }

        $form = $this->createEditForm($request, $id);
        /** @var \Kisphp\FrameworkAdminBundle\Entity\AdminUser $entity */
        $formPass = $this->createForm(UserPasswordForm::class, null, [
            'current_user' => $this->getUser(),
            'admin_roles' => $this->getParameter('admin_roles'),
        ]);
        $formPass->handleRequest($request);

        if ($formPass->isSubmitted() && $formPass->isValid()) {
            /** @var \Kisphp\FrameworkAdminBundle\Entity\AdminUser $data */
            $data = $formPass->getData();
            $providedPassword = $data->getPassword();

            $password = $this->get('security.password_encoder')
                ->encodePassword($entity, $providedPassword)
            ;

            $entity->setPassword($password);

            $this->get(static::MODEL_NAME)->save($entity);

            return $this->redirectAfterSave($entity, 'Password was successfully saved');
        }

        if ($form->isSubmitted() && $form->isValid()) {
            return $this->processSubmittedForm($form);
        }

        return $this->render(
            '@KisphpSecurity/AdminUser/edit.html.twig',
            [
                'id' => $id,
                'entity' => $entity,
                'form' => $form->createView(),
                'form_pass' => $formPass->createView(),
                'attached' => $this->getAttachedFiles($id),
                'section' => $this->section,
                'html_editor' => static::HTML_EDITOR_ENABLED,
                'upload_type' => static::UPLOAD_TYPE,
                'images_list_url' => $this->getImagesEditUrl($id),
                'section_title' => static::SECTION_TITLE . (($id > 0) ? '.edit' : '.create'),
            ]
        );
    }

    /**
     * @param Request $request
     * @param $object
     *
     * @return \Symfony\Component\Form\FormInterface
     */
    protected function createFormObject(Request $request, $object)
    {
        $currentUser = $this->getUser();

        $formOptions = [
            'current_user' => $currentUser,
            'admin_roles' => $this->getParameter('admin_roles'),
        ];

        if ($object->getId() > 0) {
            $form = $this->createForm(AdminUserEditForm::class, $object, $formOptions);
            $form->handleRequest($request);

            return $form;
        }

        $form = $this->createForm(AdminUserCreateForm::class, $object, $formOptions);
        $form->handleRequest($request);

        return $form;
    }

    /**
     * @param Form $form
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    protected function processSubmittedForm(Form $form)
    {
        $entity = $form->getData();

        if ($entity->getId() === null) {
            $password = $this->get('security.password_encoder')
                ->encodePassword($entity, $entity->getPassword())
            ;
            $entity->setPassword($password);
        }

        $savedEntity = $this->saveObjectEntity($entity);

        return $this->redirectAfterSave($savedEntity);
    }
}
